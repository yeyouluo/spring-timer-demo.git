package com.yeyouluo.spring.timer.service;

import org.apache.log4j.Logger;

public class ReadDBTask {

	public static Logger log = Logger.getLogger(ReadDBTask.class);

	public void doSomething() {
		log.info("********");
		log.info("定时任务执行。");
	}

}
