package com.yeyouluo.spring.timer.start;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	
	public static Logger logger = LoggerFactory.getLogger(Main.class);

	public static final String config = "spring-base.xml";
	
	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(config);
		logger.info("spring timer started");
		if( ctx != null ) {
			ctx.start();
		}
	}

}
